/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.reportproject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author EliteCorps
 */
public class ArtistReport {
    private int id;
    private String name;
    private int totalQty;
    private float total;

    public ArtistReport(int id, String name, int totalQty, float total) {
        this.id = id;
        this.name = name;
        this.totalQty = totalQty;
        this.total = total;
    }

    public ArtistReport(String name, int totalQty, float total) {
        this.id = -1;
        this.name = name;
        this.totalQty = totalQty;
        this.total = total;
    }

    public ArtistReport() {
        this.id = -1;
        this.name = "";
        this.totalQty = 0;
        this.total = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ArtistReport{" + "id=" + id + ", name=" + name + ", totalQty=" + totalQty + ", total=" + total + '}';
    }
    
    public static ArtistReport fromRS(ResultSet rs) {
        ArtistReport obj = new ArtistReport();
        try {
            obj.setId(rs.getInt("ArtistId"));
            obj.setName(rs.getString("Name"));
            obj.setTotalQty(rs.getInt("TotalQTY"));
            obj.setTotal(rs.getFloat("Total"));
        } catch (SQLException ex) {
            Logger.getLogger(ArtistReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
