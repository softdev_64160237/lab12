/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.reportproject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author EliteCorps
 */
public class ArtistDao implements Dao<ArtistReport> {

    @Override
    public ArtistReport get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ArtistReport> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArtistReport save(ArtistReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArtistReport update(ArtistReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(ArtistReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ArtistReport> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<ArtistReport> getTopArtistByTotal(int limit) {
        ArrayList<ArtistReport> list = new ArrayList();
        String sql = """
                     SELECT art.*, SUM(ini.Quantity) AS TotalQTY, SUM(ini.UnitPrice*ini.Quantity) AS Total
                     FROM artists art
                     INNER JOIN albums alb ON alb.ArtistId = art.ArtistId
                     INNER JOIN tracks trk ON trk.AlbumId = alb.AlbumId
                     INNER JOIN invoice_items ini ON ini.TrackId = trk.TrackId
                     INNER JOIN invoices inv ON inv.InvoiceId = ini.InvoiceId
                     GROUP BY art.ArtistId
                     ORDER BY Total DESC
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ArtistReport obj = ArtistReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ArtistReport> getTopArtistByTotal(String begin, String end, int limit) {
        ArrayList<ArtistReport> list = new ArrayList();
        String sql = """
                     SELECT art.*, SUM(ini.Quantity) AS TotalQTY, SUM(ini.UnitPrice*ini.Quantity) AS Total
                     FROM artists art
                     INNER JOIN albums alb ON alb.ArtistId = art.ArtistId
                     INNER JOIN tracks trk ON trk.AlbumId = alb.AlbumId
                     INNER JOIN invoice_items ini ON ini.TrackId = trk.TrackId
                     INNER JOIN invoices inv ON inv.InvoiceId = ini.InvoiceId
                     AND inv.InvoiceDate BETWEEN ? AND ?
                     GROUP BY art.ArtistId
                     ORDER BY Total DESC
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ArtistReport obj = ArtistReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
