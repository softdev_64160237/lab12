/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.reportproject;

import java.util.List;

/**
 *
 * @author EliteCorps
 */
public class ArtistService {
    ArtistDao artistDao = new ArtistDao();
    public List<ArtistReport> getTopArtistByTotal() {
        return artistDao.getTopArtistByTotal(10);
    }
    
    public List<ArtistReport> getTopArtistByTotal(String begin, String end) {
        return artistDao.getTopArtistByTotal(begin, end, 10);
    }
}
